package game;

import main.Logger;
import servers.GameServer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Player {

    Socket socket;
    BufferedReader reader;
    DataOutputStream output;

    public Player(Socket s){
        socket = s;

        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new DataOutputStream(socket.getOutputStream());
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public Coords getMove(){
        Coords c = null;

        boolean received = false;

        while(!received){
            try {
                writeLineToOutput("MV");
                String line = readLineFromInput();

                if (line.equals("MV")) {
                    String x = readLineFromInput();
                    String y = readLineFromInput();

                    c = new Coords(Integer.parseInt(x), Integer.parseInt(y));
                    received = true;

                    GameServer.logger.debug("Received coords: " + c);
                }
            } catch(NumberFormatException e){
                GameServer.logger.warn("NumberFormatException for received coords");
            }
        }

        return c;
    }

    public void sendValidMove(){
        writeLineToOutput("MV_VALID");
    }

    public void sendInvalidMove(){
        writeLineToOutput("MV_INVALID");
    }

    public void sendMove(Coords move){
        boolean received = false;

        while(!received) {
            writeLineToOutput("UPD");
            writeLineToOutput("" + move.x);
            writeLineToOutput("" + move.y);

            String response = readLineFromInput();

            if(response.equals("OK")){
                received = true;
                GameServer.logger.debug("Sent coords " + move);
            }
        }
    }

    public void sendLose(){
        writeLineToOutput("YOU_LOSE");
    }

    public void sendWin(){
        writeLineToOutput("YOU_WIN");
    }

    public void sendSign(Game.FieldValue sign){
        writeLineToOutput(sign.toString());
    }

    public void sendGameId(int gameId){
        writeLineToOutput("" + gameId);
    }

    private String readLineFromInput(){
        String line = null;

        try {
            line = reader.readLine();
        } catch(IOException e){
            e.printStackTrace();
        }

        return line;
    }

    private void writeLineToOutput(String line){
        try {
            output.writeBytes(line + "\n");
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public void closeStreams(){
        try {
            if(reader != null){
                reader.close();
            }

            if(output != null){
                output.close();
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
