package game;

import game.Player;
import main.KolkoKrzyzykServer;
import main.Logger;
import servers.GameServer;

import java.net.Socket;

public class Game implements Runnable {

    public enum FieldValue {
        X, O, EMPT
    }

    public enum GameState {
        SEND_INFO, ONE_MOVES, TWO_MOVES, FINISHED
    }
    private Socket playerOneSocket, playerTwoSocket;
    private Player playerOne, playerTwo;
    private FieldValue[][] fields;
    private byte gameId;
    private static byte gameCount = 0;

    private GameServer gameServer;

    public static final int SIDE_LENGTH = 3;

    public Game(Socket c, GameServer gs){
        playerOne = new Player(c);
        playerTwo = null;

        fields = new FieldValue[SIDE_LENGTH][SIDE_LENGTH];

        // Reset pól
        for(int i=0; i<fields.length; i++){
            for(int j=0; j<fields[i].length; j++){
                fields[i][j] = FieldValue.EMPT;
            }
        }

        // Nadanie ID grze
        gameCount++;
        gameId = gameCount;

        gameServer = gs;
    }

    public void setPlayerTwo(Socket c){
        playerTwo = new Player(c);
    }

    public boolean isFree(){
        return (playerTwo == null);
    }

    public byte getGameId(){ return gameId; };

    @Override
    public void run() {

        GameState state = GameState.SEND_INFO;

        loop: while(true) {
            if(playerOne != null && playerTwo != null) {

                GameServer.logger.debug("State is: " + state);

                switch(state){
                    case SEND_INFO:
                        playerOne.sendSign(FieldValue.X);
                        playerTwo.sendSign(FieldValue.O);

                        playerOne.sendGameId(gameId);
                        playerTwo.sendGameId(gameId);

                        state = GameState.ONE_MOVES;
                    case ONE_MOVES:
                        GameServer.logger.debug("Waiting for player one...");
                        Coords oneMove = playerOne.getMove();

                        if (validMove(oneMove)) {
                            // Ustawiamy wartość w tablicy na X
                            fields[oneMove.x][oneMove.y] = FieldValue.X;

                            // Informujemy gracza 1 że ruch został wykonany
                            playerOne.sendValidMove();

                            // Informujemy gracza 2 o ruchu gracza 1
                            playerTwo.sendMove(oneMove);

                            // Wysyłamy broadcast
                            gameServer.getBroadcastServer().broadcastGameState(gameId, fields);

                            // Zmieniamy stan
                            if(gameFinished()){
                                GameServer.logger.debug("Game won by player one");
                                playerOne.sendWin();
                                playerTwo.sendLose();
                                state = GameState.FINISHED;
                            } else {
                                state = GameState.TWO_MOVES;
                            }
                        } else {
                            playerOne.sendInvalidMove();
                        }
                        break;
                    case TWO_MOVES:
                        GameServer.logger.debug("Waiting for player two...");
                        Coords twoMove = playerTwo.getMove();

                        if (validMove(twoMove)) {
                            // Ustawiamy wartość w tablicy na O
                            fields[twoMove.x][twoMove.y] = FieldValue.O;

                            // Informujemy gracza 2 że ruch został wykonany
                            playerTwo.sendValidMove();

                            // Informujemy gracza 1 o ruchu gracza 2
                            playerOne.sendMove(twoMove);

                            // Wysyłamy broadcast
                            gameServer.getBroadcastServer().broadcastGameState(gameId, fields);

                            // Zmieniamy stan
                            if(gameFinished()){
                                GameServer.logger.debug("Game won by player two");
                                playerOne.sendLose();
                                playerTwo.sendWin();
                                state = GameState.FINISHED;
                            } else {
                                state = GameState.ONE_MOVES;
                            }
                        } else {
                            playerTwo.sendInvalidMove();
                        }
                        break;
                    case FINISHED:
                        break loop;
                }
            }

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        GameServer.logger.debug("Closing streams...");
        playerOne.closeStreams();
        playerTwo.closeStreams();
    }

    private void reset(boolean[][] checkers, boolean alignTop){
        for(int i=0; i<checkers.length; i++){
            for(int j=0; j<checkers[i].length; j++){
                if(alignTop) {
                    checkers[i][j] = false;
                }
            }
        }
    }

    private boolean gameFinished(){
        FieldValue[] types = {FieldValue.X, FieldValue.O};

        int[][][] sets = {
                {
                        {0, 0}, {0, 1}, {0, 2}
                },
                {
                        {1, 0}, {1, 1}, {1, 2}
                },
                {
                        {2, 0}, {2, 1}, {2, 2}
                },
                {
                        {0, 0}, {1, 0}, {2, 0}
                },
                {
                        {0, 1}, {1, 1}, {2, 1}
                },
                {
                        {0, 2}, {1, 2}, {2, 2}
                },
                {
                        {0, 0}, {1, 1}, {2, 2}
                },
                {
                        {0, 2}, {1, 1}, {2, 0}
                }
        };


        for(FieldValue fv : types) {
            for (int i = 0; i < sets.length; i++) {
                int[][] set = sets[i];

                if(fields[set[0][0]][set[0][1]] == fv &&
                        fields[set[1][0]][set[1][1]] == fv &&
                        fields[set[2][0]][set[2][1]] == fv){
                    return true;
                }
            }
        }

        return false;
    }

    private boolean validMove(Coords move){
        if(move.x >= SIDE_LENGTH || move.y >= SIDE_LENGTH){
            return false;
        } else if(fields[move.x][move.y] != FieldValue.EMPT) {
            return false;
        } else {
            return true;
        }
    }
}
