package main;

import java.io.*;

public class Logger {

    private File logFile;
    boolean logFileSet = false;

    public enum LogLevel {
        DEBUG, WARN, ERROR
    }

    public Logger(String path){
        if(path != null) {
            logFile = new File(path);
            logFileSet = true;

            if(!logFile.exists()){
                try {
                    logFile.createNewFile();
                } catch(IOException e){
                    System.out.println("WARN: Log file doesn't exist");
                    logFileSet = false;
                }
            }

            if(!logFile.canWrite()){
                System.out.println("WARN: Log file is not writable");
                logFileSet = false;
            }
        }
    }

    public Logger(){
        this(null);
    }

    public synchronized void log(LogLevel level, String message){
        if(logFileSet){
            try {
                FileOutputStream fos = new FileOutputStream(logFile, true);
                BufferedOutputStream bos = new BufferedOutputStream(fos);
                OutputStreamWriter osw = new OutputStreamWriter(bos);

                osw.write(formatMessage(level, message));

                osw.close();
            } catch(IOException e){
                System.out.println(formatMessage(level, message));
            }
        } else {
            System.out.println(formatMessage(level, message));
        }
    }

    public synchronized void debug(String message){
        log(LogLevel.DEBUG, message);
    }

    public synchronized void warn(String message){
        log(LogLevel.WARN, message);
    }

    public synchronized void error(String message){
        log(LogLevel.ERROR, message);
    }

    public String formatMessage(LogLevel level, String message){
        return level + ": " + message + "\n";
    }
}
