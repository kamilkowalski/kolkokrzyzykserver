package main;

import servers.ChatServer;
import servers.GameServer;

public class KolkoKrzyzykServer {

    public static void main(String args[]){
        KolkoKrzyzykServer server = new KolkoKrzyzykServer();
    }

    public KolkoKrzyzykServer(){
        GameServer gs = new GameServer();
        ChatServer cs = new ChatServer();

        Thread gt = new Thread(gs);
        gt.start();

        Thread ct = new Thread(cs);
        ct.start();
    }
}
