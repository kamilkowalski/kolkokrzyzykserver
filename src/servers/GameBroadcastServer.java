package servers;

import game.Game;

import java.io.IOException;
import java.net.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GameBroadcastServer implements Runnable {

    private GameServer gameServer;
    private MulticastSocket socket;

    public static final int GAME_BROADCAST_PORT = 8070;
    public static final int GAME_OBSERVER_PORT = 8071;
    public static final String GAME_DISCOVERY_GROUP = "239.0.0.1";
    public static final String GAME_BROADCAST_GROUP = "239.0.0.2";

    public GameBroadcastServer(GameServer gs){
        gameServer = gs;
    }

    /**
     * Stan gry jest kodowany do byte[] poprzez sekwencję (0 dla pola pustego, 1 dla X, 2 dla O) o długości równej ilości pól na planszy
     * np. X - -
     *     O X -
     *     O X O
     * jest kodowane jako:
     * 100210212
     *
     * Wszystko poprzedzone jest ID gry
     *
     * @param gameId ID gry
     * @param fields tablica 2D stanu gry
     */
    public void broadcastGameState(byte gameId, Game.FieldValue[][] fields) {
        int datagramLength = Game.SIDE_LENGTH * Game.SIDE_LENGTH * 2 + 1;// Zakładamy, że tablica jest kwadratowa
        byte[] sendData = new byte[datagramLength];

        emptyArray(sendData);

        sendData[0] = gameId;

        // Przepisujemy pola na dane
        int k = 1;

        for(int i=0; i<fields.length; i++) {
            for (int j = 0; j < fields[i].length; j++) {
                byte val = 0;

                switch (fields[i][j]) {
                    case X:
                        val = 1;
                        break;
                    case O:
                        val = 2;
                        break;
                }

                sendData[k] = val;
                k++;
            }
        }

        try {
            DatagramPacket packet = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(GAME_BROADCAST_GROUP), GAME_OBSERVER_PORT);
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run(){
        // Zbieramy informacje o słuchaczach
        try {
            socket = new MulticastSocket(GAME_BROADCAST_PORT);
            socket.joinGroup(InetAddress.getByName(GAME_DISCOVERY_GROUP));

            byte[] receivedData = new byte[1];
            byte[] sendData = new byte[1024];

            byte[] localAddress = InetAddress.getLocalHost().getAddress();
            GameServer.logger.debug("Local IP: " + InetAddress.getLocalHost());

            while(true){
                // Odbieramy zapytania o adres IP i dane serwera
                DatagramPacket receivedPacket = new DatagramPacket(receivedData, receivedData.length);
                socket.receive(receivedPacket);

                GameServer.logger.debug("Received data: ");

                for(byte b : receivedData){
                    GameServer.logger.debug("" + b);
                }

                // Format wiadomości zwrotnej: 4 bajty dla IP, kolejne niezerowe bajty - identyfikatory gier
                emptyArray(sendData);

                for(int i=0; i<4; i++){
                    sendData[i] = localAddress[i];
                }

                // Dodajemy do wiadomości ID gier
                List<Game> games = gameServer.getGames();
                for(int j=0; j<games.size(); j++){
                    sendData[4+j] = games.get(j).getGameId();
                }

                GameServer.logger.debug("Received packet addr: " + receivedPacket.getAddress());
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, receivedPacket.getAddress(), GAME_OBSERVER_PORT);
                socket.send(sendPacket);
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    private void emptyArray(byte[] ary){
        for(int i=0; i<ary.length; i++){
            ary[i] = 0;
        }
    }
}
