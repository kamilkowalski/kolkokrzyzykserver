package servers;

import game.Game;
import main.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class GameServer implements Runnable {

    List<Game> games = new ArrayList<Game>();
    GameBroadcastServer broadcastServer;

    public static final int GAME_SERVER_PORT = 8050;
    public static final Logger logger = new Logger("logs/game_server.log");

    @Override
    public void run() {
        try {
            // Tworzymy wątek broadcastujący gry
            broadcastServer = new GameBroadcastServer(this);
            Thread broadcastThread = new Thread(broadcastServer);
            broadcastThread.start();

            ServerSocket gameServerSocket = new ServerSocket(GAME_SERVER_PORT);

            while(true) {

                logger.debug("Accepting connections on port " + GAME_SERVER_PORT + "...");

                // Czekamy na połączenie i przydzielamy je do nowej lub istniejącej gry
                Socket gameConnection = gameServerSocket.accept();

                Game existingGame = getFreeGame();

                if (existingGame != null) {
                    logger.debug("Adding player to game #" + existingGame.getGameId());
                    existingGame.setPlayerTwo(gameConnection);
                } else {
                    // Tworzymy nową grę
                    Game newGame = new Game(gameConnection, this);

                    logger.debug("Starting new game #" + newGame.getGameId());
                    games.add(newGame);

                    // Odpalamy nowy wątek
                    Thread gameThread = new Thread(newGame);
                    gameThread.start();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public GameBroadcastServer getBroadcastServer(){
        return broadcastServer;
    }

    public List<Game> getGames(){
        return games;
    }

    private Game getFreeGame(){
        for(Game g : games){
            if(g.isFree()){
                return g;
            }
        }

        return null;
    }
}
