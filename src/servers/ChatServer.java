package servers;

import chat.Chat;
import main.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class ChatServer implements Runnable {

    Set<Chat> chats = new HashSet<Chat>();
    public static final int CHAT_SERVER_PORT = 8060;

    public static final Logger logger = new Logger("logs/chat_server.log");

    @Override
    public void run() {
        try {
            ServerSocket chatServerSocket = new ServerSocket(CHAT_SERVER_PORT);

            while(true){
                Socket chatConnection = chatServerSocket.accept();

                logger.debug("Accepting chat connection...");

                BufferedReader reader = new BufferedReader(new InputStreamReader(chatConnection.getInputStream()));

                int chatGameId = Integer.parseInt(reader.readLine());

                logger.debug("Adding client to chat for game #" + chatGameId);

                Chat ch = getChatByGameId(chatGameId);

                if(ch == null){
                    logger.debug("Chat not found, creating chat for game #" + chatGameId);

                    ch = new Chat(chatGameId);
                    chats.add(ch);
                }

                ch.addParticipant(chatConnection);

            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    private Chat getChatByGameId(int id){
        for(Chat c : chats){
            if(c.getGameId() == id){
                return c;
            }
        }

        return null;
    }
}
