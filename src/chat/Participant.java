package chat;

import servers.ChatServer;
import servers.GameServer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Participant implements Runnable {

    private Socket connection;
    private Chat chat;
    private BufferedReader reader;
    private DataOutputStream output;
    private int id;
    private List<Message> messagesSent = new ArrayList<Message>();

    public Participant(Chat ch, Socket c, int i){
        chat = ch;
        connection = c;
        id = i;

        ChatServer.logger.debug("Creating thread for participant #" + id);

        try {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            output = new DataOutputStream(connection.getOutputStream());
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while(true){
                String msg = reader.readLine();

                if(msg != null) {
                    ChatServer.logger.debug("Received chat message: " + msg);
                    chat.addMessage(msg, id);
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    public int getId(){
        return id;
    }

    public DataOutputStream getOutputStream(){
        return output;
    }

    public List<Message> getMessagesSent(){
        return messagesSent;
    }

    public void addSentMessage(Message msg){
        messagesSent.add(msg);
    }
}
