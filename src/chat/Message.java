package chat;

import java.util.Date;

public class Message {

    private String message;
    private int author;
    private Date createdAt;

    public Message(String msg, int a, Date d){
        message = msg;
        author = a;
        createdAt = d;
    }

    public Message(String msg, int a){
        this(msg, a, new Date());
    }

    public String getMessage(){
        return message;
    }

    public int getAuthor(){
        return author;
    }

    public Date getCreatedAt(){
        return createdAt;
    }
}
