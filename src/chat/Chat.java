package chat;

import servers.ChatServer;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Chat {

    private Set<Participant> participants = new HashSet<Participant>();
    private List<Message> messages = new ArrayList<Message>();
    private int gameId;
    private static int participantCount = 0;

    public Chat(int gid){
        gameId = gid;
    }

    public int getGameId(){
        return gameId;
    }

    public void addParticipant(Socket c){
        Participant p = new Participant(this, c, ++participantCount);
        participants.add(p);

        Thread th = new Thread(p);
        th.start();
    }

    public synchronized void addMessage(Message msg){
        messages.add(msg);
        ChatServer.logger.debug("Propagating chat messages");
        pushMessages();
    }

    public synchronized void addMessage(String msg, int author){
        addMessage(new Message(msg, author));
    }

    private void pushMessages(){
        ChatServer.logger.debug("Pushing messages to " + participants.size() + " participants of chat for game #" + gameId);
        try {
            for (Participant p : participants) {
                DataOutputStream out = p.getOutputStream();
                List<Message> messagesSent = p.getMessagesSent();

                for (Message m : messages) {
                    if (!messagesSent.contains(m)) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss'|'");
                        out.writeBytes("" + m.getAuthor() + "|");
                        out.writeBytes(sdf.format(m.getCreatedAt()));
                        out.writeBytes(m.getMessage() + "\n");

                        p.addSentMessage(m);
                    }
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
